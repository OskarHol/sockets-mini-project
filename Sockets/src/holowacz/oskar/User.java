package holowacz.oskar;

import java.util.ArrayList;
import java.util.HashMap;

public class User {
    private String login;
    private String password;
    private static ArrayList<User> arrayList = new ArrayList<>();
    private String rights;
    private Character[] messages = new Character[5];
    Mailbox mailbox;
    private static final int MESSAGE_LENGHT = 255;

    User(String login, String password, String permissions) {
        this.login = login;
        this.password = password;
        this.rights = permissions;
        arrayList.add(this);

        for(int i = 0; i < 5; i++) {
            //Character.SIZE = MESSAGE_LENGHT;
        }
        this.mailbox = new Mailbox();
    }

    public String getLogin (){
        return this.login;
    }

    public String getPass () {
        return this.password;
    }

    public String getRole() { return this.rights; }

    public int isMalboxFull() {
        return this.mailbox.getMailboxSize();
    }

    public boolean isAdmin() {
        if(this.rights.equals("admin")) {
            return true;
        }
        return false;
    }

    public void setPass(String password) {
        this.password = password;
    }
}
