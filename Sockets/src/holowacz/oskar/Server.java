package holowacz.oskar;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Server {
    private ServerSocket socket;
    private Socket incomingConnection;
    private static String help = "Commands: uptime - Servers up time     info - Version and created date     help - Commands description" +
            "     stop - stopping client and server    login - to log in into your account";
    private GsonBuilder builder;
    private Gson gson;
    private static float serverVersion = 0.1f;

    /*Variables for counting time*/
    private long startTime = System.nanoTime();
    private long endTime;
    private long totalTime;
    String pattern = "yyyy-MM-dd";
    private SimpleDateFormat currentDate = new SimpleDateFormat(pattern);
    private String date;
    private BufferedReader in;
    private PrintWriter out;
    public ArrayList<User> userList = new ArrayList<>();
    private int foundUserLoginIndex;


    public Server() throws IOException {
        date = currentDate.format(new Date());
        float Uptime = java.util.Calendar.getInstance().getTime().getSeconds();
        socket = new ServerSocket(8189);
        incomingConnection = socket.accept();
    }

    public void stop() throws IOException {
        this.in.close();
        this.out.close();
        this.incomingConnection.close();
        this.socket.close();
    }


    public void listen() throws IOException {
        try {

            out = new PrintWriter(incomingConnection.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(incomingConnection.getInputStream()));
            builder = new GsonBuilder();
            gson = builder.create();
            boolean done = false;

            done = userExist();

            welcomeMenu();

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            incomingConnection.close();
        }
    }

    public void welcomeMenu() throws IOException {
        boolean done = false;       //to go into the loop below - to check one of the function
        out.println("Welcome! You are in welcome screen. Type help to display commands You can use.");

        while (!done) {
            String line = in.readLine();
            switch (line) {
                case "uptime":
                    endTime = System.nanoTime();
                    totalTime = (endTime - startTime) / 1000000000;
                    String uptime = gson.toJson("uptime: " + totalTime + " seconds");
                    out.println(uptime);
                    break;

                case "info":
                    String info = gson.toJson("Info: " + serverVersion + "   " + date);
                    out.println(info);
                    break;

                case "help":
                    String helpString = gson.toJson(help);
                    out.println(helpString);
                    break;

                case "stop":
                    done = true;
                    String stop = gson.toJson("stop: stopping server and client");
                    out.println(stop);
                    this.stop();
                    break;

                case "login":
                    userExist();
                    break;
            }
        }
    }

    public boolean userExist() throws IOException {
        String login;
        String pass;
        String role;
        builder = new GsonBuilder();
        gson = builder.create();

        out.println("Do you have account already? y/n");
        String line = in.readLine();

        if (line.equals("n")) {
            out.println("Creating Account - Please provide your login: ");
            login = in.readLine();
            out.println("Please provide your password: ");
            pass = in.readLine();

            out.println("What kind of role do You want to have? regular/admin: ");
            role = in.readLine().toLowerCase();

           /*if(!role.equals("regular") || !role.equals("admin")) {
               out.println("What kind of role do You want to have? regular/admin: ");
               role = in.readLine().toLowerCase();
           }*/
            User user = new User(login, pass, role);
            String json = gson.toJson(user);

            saveUserToFile(json, true);
            return false;
        } else {
            out.println("Please provide your login: ");
            login = in.readLine();
            out.println("Please provide your password: ");
            pass = in.readLine();

            userAuthorization(login, pass);
            return true;
        }
    }

    public void saveUserToFile(String object, Boolean append) throws IOException {

        if (append) {
            try (FileWriter fw = new FileWriter("users.json", true);
                 BufferedWriter bw = new BufferedWriter(fw);
                 PrintWriter out = new PrintWriter(bw)) {

                out.println(object);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        } else {
            try (FileWriter fw = new FileWriter("users.json", false);
                 BufferedWriter bw = new BufferedWriter(fw);
                 PrintWriter out = new PrintWriter(bw)) {

                out.println(object);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    public void saveUserPassToFile(String oldString, String newString) throws IOException {

        File fileToBeModified = new File("users.json");

        String oldContent = "";

        BufferedReader reader = null;

        FileWriter writer = null;

        try {
            reader = new BufferedReader(new FileReader(fileToBeModified));
            //Reading all the lines of input text file into oldContent
            String line = reader.readLine();

            while (line != null) {
                oldContent = oldContent + line + System.lineSeparator();
                line = reader.readLine();
            }

            //Replacing oldString with newString in the oldContent
            String newContent = oldContent.replaceAll(oldString, newString);
            //Rewriting the input text file with newContent
            writer = new FileWriter(fileToBeModified);
            writer.write(newContent);
        }catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            try {
                //Closing the resources
                reader.close();
                writer.close();
            }catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void userAuthorization(String login, String password) throws IOException {
        BufferedReader bufferedReader = null;
        String file ="users.json";
        foundUserLoginIndex = -1;
        userList.clear();

        //Reading line by line from File
        StringBuilder contentBuilder = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new FileReader(file)))
        {
            String sCurrentLine;
            while ((sCurrentLine = br.readLine()) != null)
            {
                contentBuilder.append(sCurrentLine);//.append("\n");
                userList.add(gson.fromJson(sCurrentLine, User.class));
            }
        }

            for (int i = 0; i < userList.size(); i++) {
                if (userList.get(i).getLogin().equals(login) && userList.get(i).getPass().equals(password)) {
                    out.println("You have successfully log in as: " + userList.get(i).getLogin() + " User to Our System");
                    out.println();
                    foundUserLoginIndex = i;
                }
            }

        if (foundUserLoginIndex != -1) {
            UserMenu(userList.get(foundUserLoginIndex));
        }else {
            out.println("We have not found User You are trying to log in!");
            out.println();
        }
    }

    public void UserMenu(User user) throws IOException {
        boolean done = true;
        int i = 0;
        String newPass;

        while (done){
            out.println("Action which User can take: \n1.Display Users \n2.Send Message to User \n3.Read Message from Mailbox \n4.Delete Message \n5.Send" +
                    " ask to admin to reset password \n6.Delete my account \n7.Log out");
            int userAction = Integer.parseInt(in.readLine());
            switch (userAction) {
                case 1:
                    out.println("The User List:");
                    for (User u : userList) {
                        i++;
                        out.println(i + ". " + u.getLogin());
                    }
                    out.println();
                    break;
                case 2:
                    out.println("Pick up user from the list who You like to send message (Message maximum size is 255 characters; If You )");
                    int choosedUser = Integer.parseInt(in.readLine());

                    if (choosedUser > userList.size()) {
                        out.println("User does not exist");
                    }

                    //if (userList.get(choosedUser).isMalboxFull() > 5)
                      //  out.println("You can not send message to that user - his/her mailbox is full");

                    String text = in.readLine();
                    if ( text.length() > 255) {
                        text = text.substring(0,254);
                    }

                    userList.get(choosedUser).mailbox.addInformation(text);
                    String saveSendMessage = gson.toJson(userList.get(choosedUser));

                    //saveUserPassToFile(, saveSendMessage);
                    //TO DO SAVE MESSAGE TO JSON

                    break;
                case 3:
                    out.println("Which message do You want to read? (Enter number - below will be displayed the list without content: ");
                    //TODO - DISPLAY NUMBERS OF MESSAGES
                    in.readLine();
                    //out.println(userList.get(foundUserLoginIndex).mailbox);
                    break;
                case 4:
                    out.println("Which message do You want to delete (Enter number - below should be displayed the list of messages: ");
                    //userList.get(foundUserLoginIndex).mailbox);
                    break;
                case 5:
                    out.println("Your request for changing the password has been sent to Administrator!");
                    if (userList.get(foundUserLoginIndex).isAdmin()) {
                        out.println("Provide new password: ");
                        newPass = in.readLine();
                        saveUserPassToFile(userList.get(foundUserLoginIndex).getPass(),newPass);
                    } else {
                        out.println("You are going to reset your password through the Administrator.\n Please provide new password:");
                        newPass = in.readLine();
                        //For now it is instant change of password (should be better solution than sending to admin and waiting when he will change it)
                        saveUserPassToFile(userList.get(foundUserLoginIndex).getPass(),newPass);
                    }
                    break;
                case 6:
                    out.println("Are You certain You want to delete permanently your account? (y/n)");
                    String answer = in.readLine();

                    if (answer.equals("y")) {
                        out.println("Your account has been deleted. You are going to be redirected to Main MENU.\n");
                        userList.remove(foundUserLoginIndex);

                        if(userList.isEmpty()) {
                            clearFile();
                        }else{
                            for (User current : userList) {
                                String json = gson.toJson(current);
                                saveUserToFile(json, false);
                            }
                        }
                        done = false;
                        logOut(done);

                    }else {
                        out.println("Ok - your account will be still in our database");
                    }

                case 7:
                    done = false;
                    logOut(done);
                    break;
            }
        }
    }

    public void logOut(boolean done) throws IOException {
        out.println("You have been successfully log out");
        done = false;
        welcomeMenu();
    }

    public void clearFile() {

            try (FileWriter fw = new FileWriter("users.json", false);
                 BufferedWriter bw = new BufferedWriter(fw);
                 PrintWriter out = new PrintWriter(bw)) {

                out.flush();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
    }


    public static void main(String[] args) throws IOException {
        Server s = new Server();
        s.listen();
    }
}
