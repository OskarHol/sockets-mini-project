package holowacz.oskar;
import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class Client {
    private static final String SERVER_IP = "127.0.0.1";
    private static final int SERVER_PORT = 8189;
    private Socket clientSocket;
    private PrintWriter out;
    private BufferedReader in;


    public void startConnection(String ip, int port) throws IOException {
        clientSocket = new Socket(ip, port);
    }

    public void startSession() throws IOException {
        System.out.println("Hello! We've got those command to use: uptime/info/help/stop.");

        out = new PrintWriter(clientSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        boolean done = false;

        String line;
        String response = in.readLine();
        System.out.println(response);

        while (!done) {

            line = bufferedReader.readLine();
            out.println(line);
            response = in.readLine();
            System.out.println(response);

            if(line.equals("stop")) {
                done = true;
                out.println("stop");
                System.out.println(response);
                this.stopConnection();
            }
        }
    }

    public void stopConnection() throws IOException {
        in.close();
        out.close();
        clientSocket.close();
    }

    public static void main (String[] args) throws IOException {
        Client client = new Client();
        client.startConnection(SERVER_IP, SERVER_PORT);
        client.startSession();
    }
}